import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Row, Col, Button, Form, Card, InputGroup } from 'react-bootstrap';
import InvoiceItem from './InvoiceItem';
import InvoiceModal from './InvoiceModal';

class InvoiceForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      currency: '₹',
      currentDate: '',
      invoiceNumber: 1,
      dateOfIssue: '',
      billTo: '',
      billToEmail: '',
      billToAddress: '',
      billToGst:"",
      billFrom: '',
      billFromEmail: '',
      billFromAddress: '',
      billFromGst:"",
      notes: '',
      total: '0.00',
      subTotal: '0.00',
      taxRate: '',
      taxAmount: '0.00',
      discountRate: '',
      discountAmount: '0.00',
      taxType: 'withinState', // Added state for tax type
      items: [
        {
          id: 0,
          name: '',
          description: '',
          noOfWorkingDays: '',
          price: '1000',
          quantity: 1,
          HSN:4,
        },
      ],
    };
  }

  componentDidMount() {
    this.handleCalculateTotal();
  }

  handleRowDel = (item) => {
    const items = this.state.items.filter(i => i.id !== item.id);
    this.setState({ items }, this.handleCalculateTotal);
  }

  handleAddEvent = () => {
    const id = (+new Date() + Math.floor(Math.random() * 999999)).toString(36);
    const newItem = {
      id: id,
      name: '',
      price: '1.00',
      description: '',
      quantity: 1,
    };
    this.setState({ items: [...this.state.items, newItem] });
  }

  handleCalculateTotal = () => {
    const { items, taxRate, discountRate, taxType } = this.state;
    let subTotal = 0;
    items.forEach(item => {
      subTotal += parseFloat(item.price) * parseInt(item.quantity);
    });
  
    const discountAmount = parseFloat(subTotal) * (discountRate / 100);
    subTotal -= discountAmount; // Apply discount to subtotal
  
    let cgst = 0, sgst = 0, gst = 0, totalTax = 0;
  
    if (taxType === 'withinState') {
      // Calculate CGST and SGST for 'Within State' selection
      cgst = sgst = parseFloat(subTotal) * (taxRate / 200); // Half of the total tax rate for each
      totalTax = cgst + sgst; // Total tax is the sum of CGST and SGST
    } else {
      // Apply full GST for 'Other State' selection
      gst = parseFloat(subTotal) * (taxRate / 100);
      totalTax = gst; // Total tax is just the GST amount
    }
  
    const total = subTotal + totalTax; // Total amount including tax
  
    // Update the state with the new calculated values
    this.setState({
      subTotal: subTotal.toFixed(2),
      discountAmount: discountAmount.toFixed(2),
      cgst: cgst.toFixed(2),
      sgst: sgst.toFixed(2),
      gst: gst.toFixed(2), // Add this to the state if you want to display GST separately
      taxAmount: totalTax.toFixed(2),
      total: total.toFixed(2),
    });
  };
  
  

  onItemizedItemEdit = (evt, itemId) => {
    const { name, value } = evt.target;
  
    // Map through items and update the item that matches the itemId
    const updatedItems = this.state.items.map(item => {
      if (item.id === itemId) {
        return { ...item, [name]: value };
      }
      return item;
    });
  
    this.setState({ items: updatedItems }, this.handleCalculateTotal);
  }
  

  editField = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value }, this.handleCalculateTotal);
  }

  onCurrencyChange = (event) => {
    this.setState({ currency: event.target.value });
  }

  openModal = (event) => {
    event.preventDefault();
    this.handleCalculateTotal();
  
    // Prepare the data with the correct tax details
    const dataToSend = {
      // include other invoice details here
      subTotal: this.state.subTotal,
      total: this.state.total,
      taxRate: this.state.taxRate,
      discountAmount: this.state.discountAmount,
      // include the correct tax details based on taxType
      taxDetails: this.state.taxType === 'withinState' ? 
        {
          cgst: this.state.cgst,
          sgst: this.state.sgst,
        } : 
        {
          igst: this.state.taxAmount, // since gst is calculated as taxAmount in the 'otherState' case
        },
    };
  
    // Assuming you're displaying this in a modal, you could set this in the state
    // If you're sending this to a backend, you would send 'dataToSend' to your backend service here
  
    this.setState({ isOpen: true, dataToSend: dataToSend }); // Update this line based on what you're actually doing with the data
  };
  
  closeModal = () => {
    this.setState({ isOpen: false });
  }

  renderTaxDetails() {
    const { taxType, cgst, sgst, gst, currency } = this.state;
    if (taxType === 'withinState') {
      return (
        <>
          <div className="d-flex flex-row align-items-start justify-content-between mt-2">
            <span className="fw-bold">CGST ({this.state.taxRate / 2}%):</span>
            <span>{currency}{cgst}</span>
          </div>
          <div className="d-flex flex-row align-items-start justify-content-between mt-2">
            <span className="fw-bold">SGST ({this.state.taxRate / 2}%):</span>
            <span>{currency}{sgst}</span>
          </div>
        </>
      );
    } else {
      return (
        <div className="d-flex flex-row align-items-start justify-content-between mt-2">
          <span className="fw-bold">IGST ({this.state.taxRate}%):</span>
          <span>{currency}{this.state.taxAmount }</span>
        </div>
      );
    }
  }
  
  

  render() {
    return (
      <Form onSubmit={this.openModal}>
        <Row>
          <Col md={8} lg={9}>
            <Card className="p-4 p-xl-5 my-3 my-xl-4">
              
              <div className="d-flex flex-row align-items-start justify-content-between mb-3">

                <div className="d-flex flex-column">
                  <div className="mb-2">
                    <span className="fw-bold">Current Date:</span>
                    <span className="current-date">{new Date().toLocaleDateString()}</span>
                  </div>
                  <div className="d-flex flex-row align-items-center">
                    <span className="fw-bold d-block me-2">Due:</span>
                    <Form.Control type="date" value={this.state.dateOfIssue} name="dateOfIssue" onChange={this.editField} style={{ maxWidth: '150px' }} required />
                  </div>
                </div>
                <div className="d-flex flex-row align-items-center">
                <div className='d-flex flex-column justify-content-center align-items-center'>
                <img src="https://jpencil.com/img/logo.png" alt="" style={{width:"50%"}} />
              </div>

                  <span className="fw-bold me-2" >Invoice:</span>
                  <Form.Control type="number" value={this.state.invoiceNumber} name="invoiceNumber" onChange={this.editField} min="1" style={{ maxWidth: '170px' }} required />
                </div>
              </div>
              <hr className="my-4" />
              <Row className="mb-5">
  <Col>
    <Form.Label className="fw-bold">Quotation To:</Form.Label>
    <Form.Control className="mb-2" placeholder="Who is this quotation to?" value={this.state.billTo} type="text" name="billTo" onChange={this.editField} required />
    <Form.Control className="mb-2" placeholder="Email address" value={this.state.billToEmail} type="email" name="billToEmail" onChange={this.editField} required />
    <Form.Control className="mb-2" placeholder="Billing address" value={this.state.billToAddress} type="text" name="billToAddress" onChange={this.editField} required />
    <Form.Control placeholder="GST number" value={this.state.billToGst} type="text" name="billToGst" onChange={this.editField} required />
  </Col>
  <Col>
    <Form.Label className="fw-bold">Quotation By:</Form.Label>
    <Form.Control className="mb-2" placeholder="Who is this quotation from?" value={this.state.billFrom} type="text" name="billFrom" onChange={this.editField} required />
    <Form.Control className="mb-2" placeholder="Email address" value={this.state.billFromEmail} type="email" name="billFromEmail" onChange={this.editField} required />
    <Form.Control className="mb-2" placeholder="Billing address" value={this.state.billFromAddress} type="text" name="billFromAddress" onChange={this.editField} required />
    <Form.Control placeholder="GST number" value={this.state.billFromGst} type="text" name="billFromGst" onChange={this.editField} required />
  </Col>
</Row>

              <InvoiceItem items={this.state.items} onItemizedItemEdit={this.onItemizedItemEdit} onRowAdd={this.handleAddEvent} onRowDel={this.handleRowDel} currency={this.state.currency} />
              <Row className="mt-4 justify-content-end">
                <Col lg={6}>
                  <div className="d-flex flex-row align-items-start justify-content-between">
                    <span className="fw-bold">Subtotal:</span>
                    <span>{this.state.currency}{this.state.subTotal}</span>
                  </div>
                  
                 
                 
                  {/* Render Tax Details based on Tax Type */}
                  {this.renderTaxDetails()}
                  <hr />
                  <div className="d-flex flex-row align-items-start justify-content-between" style={{ fontSize: '1.125rem' }}>
                    <span className="fw-bold">Total:</span>
                    <span className="fw-bold">{this.state.currency}{this.state.total || 0}</span>
                  </div>
                </Col>
              </Row>
              <hr className="my-4" />
              <Form.Label className="fw-bold">Notes:</Form.Label>
              <Form.Control placeholder="Thanks for your business!" name="notes" value={this.state.notes} onChange={this.editField} as="textarea" rows={1} />
            </Card>
          </Col>
          <Col md={4} lg={3}>
            <div className="sticky-top pt-md-3 pt-xl-4">
              <Button variant="primary" type="submit" className="d-block w-100">Review Invoice</Button>
              <InvoiceModal showModal={this.state.isOpen} closeModal={this.closeModal} info={this.state} items={this.state.items} currency={this.state.currency} subTotal={this.state.subTotal} taxAmount={this.state.taxAmount} discountAmount={this.state.discountAmount} total={this.state.total} />
              <Form.Group className="mb-3">
                <Form.Label className="fw-bold mt-3">Currency:</Form.Label>
                <Form.Select onChange={this.onCurrencyChange} className="btn btn-light my-1" aria-label="Change Currency">
                  <option value="₹">INR (Indian Rupees)</option>
                  <option value="$">USD (United States Dollar)</option>
                </Form.Select>
              </Form.Group>
              <Form.Group className="my-3">
                <Form.Label className="fw-bold">Tax rate:</Form.Label>
                <InputGroup className="my-1 flex-nowrap">
                  <Form.Control name="taxRate" type="number" value={this.state.taxRate} onChange={this.editField} className="bg-white border" placeholder="0.0" min="0" step="0.01" max="100" />
                  <InputGroup.Text className="bg-light fw-bold text-secondary small">%</InputGroup.Text>
                </InputGroup>
              </Form.Group>
               {/* Tax Type Radio Buttons */}
               <Form.Group className="mt-3">
                    <Form.Label className="fw-bold">Tax Type:</Form.Label>
                    <div>
                      <Form.Check inline label="Within State" type="radio" name="taxType" id="withinState" checked={this.state.taxType === 'withinState'} onChange={() => this.setState({ taxType: 'withinState' })} />
                      <Form.Check inline label="Other State" type="radio" name="taxType" id="otherState" checked={this.state.taxType === 'otherState'} onChange={() => this.setState({ taxType: 'otherState' })} />
                    </div>
                  </Form.Group>
            </div>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default InvoiceForm;
