import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';

class EditableField extends React.Component {
  handleChange = (e) => {
    // Call the passed onItemizedItemEdit function with event and id
    this.props.onItemizedItemEdit(e, this.props.cellData.id);
  }

  render() {
    return (
      <InputGroup className="my-1 flex-nowrap">
        {this.props.cellData.leading && (
          <InputGroup.Text className="bg-light fw-bold border-0 text-secondary px-2">
            <span className="border border-2 border-secondary rounded-circle d-flex align-items-center justify-content-center small" style={{ width: '20px', height: '20px' }}>
              {this.props.cellData.leading}
            </span>
          </InputGroup.Text>
        )}
        <Form.Control
          className={this.props.cellData.textAlign}
          type={this.props.cellData.type}
          placeholder={this.props.cellData.placeholder}
          min={this.props.cellData.min}
          name={this.props.cellData.name}
          id={this.props.cellData.id}
          value={this.props.cellData.value}
          step={this.props.cellData.step}
          precision={this.props.cellData.precision} // Corrected spelling from 'presicion' to 'precision'
          aria-label={this.props.cellData.name}
          onChange={this.handleChange} // Use the handleChange method
          required
        />
      </InputGroup>
    );
  }
}

export default EditableField;

