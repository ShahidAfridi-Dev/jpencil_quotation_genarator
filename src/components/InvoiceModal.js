import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';

import { BiPaperPlane, BiCloudDownload } from "react-icons/bi";
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';

function GenerateInvoice(invoiceNumber, captureElementId) {
  const input = document.querySelector(`#${captureElementId}`);
  // Temporarily hide the button container before capture
  const buttonContainer = document.getElementById('buttonContainer'); // Make sure you have an element with this id
  buttonContainer.style.display = 'none';

  // Load the header image
  const headerImg = new Image();
  headerImg.src = "/images/invoice_header.jpg"; // Path to the header image
  headerImg.onload = () => {
    html2canvas(input, {
      useCORS: true,
      scale: 2, // Adjust scale as needed
    }).then((canvas) => {
      const imgWidth = 210; // A4 width in mm
      const pageHeight = 295; // A4 height in mm, adjusted for margin
      const imgHeight = (canvas.height * imgWidth) / canvas.width;
      let heightLeft = imgHeight;

      const pdf = new jsPDF("p", "mm");
      let position = 0;

      // Add the header image to the PDF
      pdf.addImage(headerImg, 'JPEG', 0, position, imgWidth, 20); // Adjust the height as needed
      position += 20; // Adjust position for the main content

      // Add the main content from canvas
      pdf.addImage(canvas.toDataURL("image/png"), 'PNG', 0, position, imgWidth, imgHeight);
      heightLeft -= pageHeight;

      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        pdf.addPage();

        // Add the header image again for the new page
        pdf.addImage(headerImg, 'JPEG', 0, 0, imgWidth, 20); // Header at the top of the new page
        position = 20; // Reset position for main content on the new page

        pdf.addImage(canvas.toDataURL("image/png"), 'PNG', 0, position, imgWidth, imgHeight);
        heightLeft -= pageHeight;
      }

      // Save the PDF
      pdf.save(`invoice-${invoiceNumber}.pdf`);

      // Show the button container again after saving the PDF
      buttonContainer.style.display = '';
    });
  };

  headerImg.onerror = () => {
    // Show the button container again if there is an error
    buttonContainer.style.display = '';
    console.error("Error loading header image. Ensure the path to the image is correct.");
  };
}




class InvoiceModal extends React.Component {
  
  renderTaxDetails() {
    console.log("invoiceModal",this.props);
    const { taxType, cgst, sgst, taxAmount } = this.props.info;
    if (taxType === 'withinState') {
      return (
        <>
          <tr className="text-end">
            <td></td>
            <td className="fw-bold">CGST ({this.props.info.taxRate / 2}%)</td>
            <td className="text-end">{this.props.currency} {cgst}</td>
          </tr>
          <tr className="text-end">
            <td></td>
            <td className="fw-bold">SGST ({this.props.info.taxRate / 2}%)</td>
            <td className="text-end">{this.props.currency} {sgst}</td>
          </tr>
        </>
      );
    } else {
      return (
        <tr className="text-end">
          <td></td>
          <td className="fw-bold">IGST ({this.props.info.taxRate}%)</td>
          <td className="text-end">{this.props.currency} {taxAmount}</td>
        </tr>
      );
    }
  }

  render() {
    return (
      <div>
        <Modal show={this.props.showModal} onHide={this.props.closeModal} size="lg" centered>
          <div id="invoiceCapture">
            <div className="d-flex flex-row justify-content-between align-items-start bg-light  p-4">
              <div className="w-100">
                <h4 className="fw-bold my-2">{this.props.info.billFrom || 'J Pencil'}</h4>
                <h6 className="fw-bold text-secondary mb-1">
                  Order Quotation #: {this.props.info.invoiceNumber || ''}
                </h6>
              </div>
              <div className='d-flex flex-column justify-content-center align-items-center'>
                <img src="https://jpencil.com/img/logo.png" alt="" style={{ width: "50%", marginLeft: "-160px" }} />
              </div>
          
            </div>
            <div className="p-4">
              <Row className="mb-4">
                <Col md={4}>
                  <div className="fw-bold">Quoted for :</div>
                  <div>{this.props.info.billTo || ''}</div>
                  <div>{this.props.info.billToAddress || ''}</div>
                  <div>{this.props.info.billToEmail || ''}</div>
                  <div> GST :{this.props.info.billToGst || ''}</div>
                </Col>
                <Col md={4}>
                  <div className="fw-bold">Quoted by :</div>
                  <div>{this.props.info.billFrom || ''}</div>
                  <div>{this.props.info.billFromAddress || ''}</div>
                  <div>{this.props.info.billFromEmail || ''}</div>
                  <div> GST: {this.props.info.billFromGst || ''}</div>
                </Col>
                <Col md={4}>
  <div className="fw-bold "> Date of Quotation:</div>
  <div>{new Date().toLocaleDateString() || ''}</div>
  <div className="fw-bold mt-2">Due Date of Quotation:</div>
  <div>{new Date(this.props.info.dateOfIssue).toLocaleDateString() || ''}</div>
</Col>

                
              </Row>
              <Table className="mb-0">
                <thead>
                  <tr>
                    <th>QTY</th>
                    <th>PRODUCT</th>
                    <th>HSN/SAC</th>
                    <th>No of Working days</th>
                    <th className="text-end">PRICE</th>
                    <th className="text-end">AMOUNT</th>
                  </tr>
                </thead>
                <tbody>
                  {this.props.items.map((item, i) => (
                    <tr key={i}>
                      <td style={{ width: '70px' }}>{item.quantity}</td>
                      <td>{item.name} - {item.description}</td>
                      <td> {item.HSN}</td>
                      <td> {item.noOfWorkingDays}</td>
                      <td className="text-end" style={{ width: '100px' }}>{this.props.currency} {item.price}</td>
                      <td className="text-end" style={{ width: '100px' }}>{this.props.currency} {parseFloat(item.price) * item.quantity}</td>
                    </tr>
                  ))}
                </tbody>
              </Table>
              <Table>
                <tbody>
                  <tr className="text-end">
                    <td></td>
                    <td className="fw-bold">SUBTOTAL</td>
                    <td className="text-end">{this.props.currency} {this.props.subTotal}</td>
                  </tr>
                  {this.renderTaxDetails()}
                  <tr className="text-end">
                    <td></td>
                    <td className="fw-bold">TOTAL</td>
                    <td className="text-end">{this.props.currency} {this.props.total}</td>
                  </tr>
                </tbody>
              </Table>
              {this.props.info.notes && (
                <div className="bg-light py-3 px-4 rounded">{this.props.info.notes}</div>
              )}
            </div>
            <div className="pb-4 px-4" >
              <Row>
                
                <Col md={6} id="buttonContainer">
                <Button id="downloadButton" variant="outline-primary" className="d-block w-100 mt-3 mt-md-0" onClick={() => GenerateInvoice(this.props.info.invoiceNumber, "invoiceCapture")}>
  <BiCloudDownload style={{ width: '16px', height: '16px', marginTop: '-3px' }} className="me-2"/>Download Copy
</Button>


                </Col>
              </Row>
              <Row>
                
                <span><br/>This is a quotation and not an invoice or GST invoice. The work will begin only after the quotation is signed by an authorised signatory and sent to app@jpencil.com The number of working days mentioned will be the number of business days as per the government calendar. Changes to the due date may occur and the same will be intimated as and when decided.</span>
              </Row>
              <Row style={{marginTop:"40px"}}>
    <Col md={{ span: 3, offset: 3 }}>
      <Form.Check 
        type="checkbox"
        id="acceptCheckbox"
        label="Accept"
      />
    </Col>
    <Col md={3}>
      <Form.Check 
        type="checkbox"
        id="rejectCheckbox"
        label="Reject"
      />
    </Col>
  </Row>
  <Row className="mt-2 justify-content-end">
    <Col md={4}>
      <div className="text-end mt-3">
        <div>Authorized by</div>
        <div>_________________________</div> {/* Signature line */}
        {/* <Form.Control type="text" placeholder="Enter name" className="mt-2 text-end" style={{border: 'none', borderBottom: '1px solid black', borderRadius: '0'}} /> */}
      </div>
    </Col>
  </Row>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

export default InvoiceModal;
